/*
 * GStreamer
 * Copyright (C) 2005 Thomas Vander Stichele <thomas@apestaart.org>
 * Copyright (C) 2005 Ronald S. Bultje <rbultje@ronald.bitfreak.net>
 * Copyright (C) 2014 c_kamuju <<user@hostname.org>>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Alternatively, the contents of this file may be used under the
 * GNU Lesser General Public License Version 2.1 (the "LGPL"), in
 * which case the following provisions apply instead of the ones
 * mentioned above:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

/**
 * SECTION:element-qcvideodec
 *
 * FIXME:Describe qcvideodec here.
 *
 * <refsect2>
 * <title>Example launch line</title>
 * |[
 * gst-launch -v -m fakesrc ! qcvideodec ! fakesink silent=TRUE
 * ]|
 * </refsect2>
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include "gstqcvideodec.h"
#include "gstqcvideobufpool.h"

GST_DEBUG_CATEGORY_STATIC (gst_qcvideodec_debug);
#define GST_CAT_DEFAULT gst_qcvideodec_debug

/* Filter signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  PROP_0,
  PROP_SILENT
};

/* the capabilities of the inputs and outputs.
 *
 * describe the real formats here.
 */
static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/mpeg, "
        "mpegversion=(int) 4, "
        "systemstream=(boolean) false, "
        "width=(int) [1,2048], "
        "height=(int) [1,2048]"
        ";"
        "video/x-divx, "
        "divxversion = (int)[3, 5], "
        "width=(int) [1,2048], "
        "height=(int) [1,2048]"
        ";"
        "video/x-h264, "
        "parsed=(boolean)true, "
        "alignment=(string) au, "
        "stream-format=(string) byte-stream, "
        "width=(int) [1,2048], " "height=(int) [1,2048]")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("video/x-raw, "
        "format = (string) NV12_64Z32, "
        "   framerate=(fraction)[ 0, MAX ], "
        "   width=(int)[ 1, MAX ], " "   height=(int)[ 1, MAX ]")
    );

#define gst_qcvideodec_parent_class parent_class
G_DEFINE_TYPE (Gstqcvideodec, gst_qcvideodec, GST_TYPE_VIDEO_DECODER);

static void gst_qcvideodec_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec);
static void gst_qcvideodec_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec);

static gboolean gst_qcvideodec_open (GstVideoDecoder * decoder);
static gboolean gst_qcvideodec_start (GstVideoDecoder * decoder);
static gboolean gst_qcvideodec_stop (GstVideoDecoder * decoder);
static gboolean gst_qcvideodec_set_format (GstVideoDecoder * decoder,
    GstVideoCodecState * state);
static gboolean gst_qcvideodec_close (GstVideoDecoder * decoder);
static GstFlowReturn gst_qcvideodec_handle_frame (GstVideoDecoder * decoder,
    GstVideoCodecFrame * frame);
static GstFlowReturn gst_qcvideodec_finish (GstVideoDecoder * decoder);
//static GstFlowReturn gst_qcvideodec_parse(GstVideoDecoder *decoder,GstVideoCodecFrame *frame,GstAdapter *adapter,gboolean at_eos);
static gboolean gst_qcvideodec_create_buf_pool (Gstqcvideodec * decoder);
static GstVideoCodecFrame *gst_qc_video_dec_get_oldest_frame (GstVideoDecoder *
    decoder);

static void *video_thread (void *);
static gboolean gst_qcvideodec_start_decoder (Gstqcvideodec * decoder);
static GstFlowReturn gst_qcvideodec_decode_buffer (Gstqcvideodec *
    decode_context, GstBuffer * buf, GstVideoCodecFrame * frame);
static int ion_allocate_buffer (enum vdec_buffer buffer_dir,
    Gstqcvideodec * decoder_cxt);
int free_buffer (enum vdec_buffer buffer_dir, Gstqcvideodec * decoder_context);
static gboolean gst_qcvideodec_set_output_buffers (Gstqcvideodec * decoder);
int vdec_alloc_h264_mv (Gstqcvideodec * decoder_cxt);

int reconfig = 0;
static struct vdec_bufferpayload *temp_input_buffer = NULL;
gboolean Completed_frm_decode = FALSE;
static GstFlowReturn
gst_qcvideodec_finish (GstVideoDecoder * decoder)
{
  int i = 0;
  struct vdec_input_frameinfo frameinfo;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };

  GST_VIDEO_DECODER_STREAM_UNLOCK (dec);

  for (i = 0; i < dec->output_buffer.mincount; i++) {
    GST_VIDEO_DECODER_STREAM_LOCK (dec);
    frameinfo.flags |= VDEC_BUFFERFLAG_EOS;
    frameinfo.bufferaddr = temp_input_buffer->bufferaddr;
    frameinfo.offset = 0;
    frameinfo.pmem_fd = temp_input_buffer->pmem_fd;
    frameinfo.pmem_offset = temp_input_buffer->offset;
    frameinfo.datalen = 0;
    frameinfo.client_data = (struct vdec_bufferpayload *)
        temp_input_buffer;

    ioctl_msg.in = &frameinfo;
    ioctl_msg.out = NULL;
    if (Completed_frm_decode == TRUE) {
      GST_VIDEO_DECODER_STREAM_UNLOCK (dec);
      break;
    }
    if (ioctl (dec->video_driver_fd, VDEC_IOCTL_DECODE_FRAME, &ioctl_msg) < 0) {
      GST_ERROR ("VDEC_IOCTL_DECODE_FRAME Failed ");
      break;
    }
    GST_VIDEO_DECODER_STREAM_UNLOCK (dec);
    sem_wait (&dec->sem_input_buf_done);
  }                             /* for loop */
  GST_DEBUG ("gst_qcvideodec_finish returned : Exit");
  return GST_FLOW_OK;
}

static gboolean
gst_qcvideodec_open (GstVideoDecoder * decoder)
{
  pthread_mutexattr_t init_values;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);

  dec->bufpool = NULL;
  /* Open ion drive for memory allocations */
  dec->m_vdec_ion_devicefd = open ("/dev/ion", O_RDONLY);

  if (dec->m_vdec_ion_devicefd < 0) {
    GST_ERROR ("ION Device open() Failed");
    return FALSE;
  }

  dec->video_driver_fd = open ("/dev/msm_vidc_dec", O_RDWR | O_NONBLOCK);

  if (dec->video_driver_fd < 0) {
    GST_ERROR ("/dev/msm_vidc_dev Open failed");
    return FALSE;
  }

  sem_init (&dec->sem_synchronize, 0, 0);
  sem_init (&dec->sem_input_buf_done, 0, 0);

  pthread_mutexattr_init (&init_values);

  /*Create thread */
  if (pthread_create (&dec->videothread_id, NULL, video_thread, dec) < 0) {
    GST_ERROR ("Not able to create threads");
    return FALSE;
  }

  return TRUE;
}

static gboolean
gst_qcvideodec_start (GstVideoDecoder * decoder)
{

  return TRUE;
}

static gboolean
gst_qcvideodec_stop (GstVideoDecoder * decoder)
{
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  enum vdec_bufferflush flush_dir = VDEC_FLUSH_TYPE_INPUT;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);

  ioctl_msg.in = &flush_dir;
  ioctl_msg.out = NULL;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_CMD_FLUSH, &ioctl_msg) < 0) {
    GST_ERROR ("Flush input failed");
  } else {
    sem_wait (&dec->sem_synchronize);
  }

  flush_dir = VDEC_FLUSH_TYPE_OUTPUT;
  ioctl_msg.in = &flush_dir;
  ioctl_msg.out = NULL;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_CMD_FLUSH, &ioctl_msg) < 0) {
    GST_ERROR ("Flush output failed");
  } else {
    sem_wait (&dec->sem_synchronize);
  }

  GST_DEBUG ("I/p and O/p ION buffers FLUSHED properly");
  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_CMD_STOP, NULL) < 0) {
    GST_ERROR ("Stop failed");
  } else {
    sem_wait (&dec->sem_synchronize);
    GST_DEBUG ("VDEC driver STOPPED successfully");
  }
  /* Release the buffers in the pool and then pool */
  gst_buffer_pool_set_active (dec->bufpool, FALSE);
  gst_object_unref (dec->bufpool);
  if (dec->input_state) {
    gst_video_codec_state_unref (dec->input_state);
    dec->input_state = NULL;
  }
  if (dec->output_state) {
    gst_video_codec_state_unref (dec->output_state);
    dec->output_state = NULL;
  }
  return TRUE;
}

static gboolean
gst_qcvideodec_set_format (GstVideoDecoder * decoder,
    GstVideoCodecState * state)
{
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  enum vdec_output_fromat output_format = VDEC_YUV_FORMAT_TILE_4x2;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);
  GstBuffer *buffer;
  GstMapInfo minfo = GST_MAP_INFO_INIT;
  struct vdec_input_frameinfo frameinfo;
  GstVideoInfo *Vinfo;
  GstCaps *caps;
  gchar *caps_data;

  GstStructure *structure;
  GstVideoFormat fmt = GST_VIDEO_FORMAT_NV12_64Z32;

  /* Keep a copy of the input state */
  if (dec->input_state)
    gst_video_codec_state_unref (dec->input_state);
  dec->input_state = gst_video_codec_state_ref (state);

  caps = state->caps;
  buffer = state->codec_data;
  Vinfo = &state->info;
  gst_buffer_map (buffer, &minfo, GST_MAP_READ);
  caps_data = gst_caps_to_string (caps);
  caps = gst_caps_make_writable (gst_pad_query_caps (dec->srcpad, NULL));

  gst_pad_set_caps (dec->srcpad, caps);
  gst_caps_unref (caps);
  GST_DEBUG ("gst_qcvideodec_set_format Enter: caps_data : %s ", caps_data);
  GST_DEBUG ("gst_qcvideodec_set_format Enter: data_size : %d ", minfo.size);
  GST_DEBUG ("gst_qcvideodec_set_format Enter: width : %d , Height : %d ",
      Vinfo->width, Vinfo->height);

  structure = gst_caps_get_structure (state->caps, 0);

  if (gst_structure_has_name (structure, "video/mpeg")) {
    dec->decoder_format = VDEC_CODECTYPE_MPEG4;
  } else if (gst_structure_has_name (structure, "video/x-divx")) {
    int version;
    gst_structure_get_int (structure, "divxversion", &version);
    switch (version) {
      case 3:
        dec->decoder_format = VDEC_CODECTYPE_DIVX_3;
        break;
      case 4:
        dec->decoder_format = VDEC_CODECTYPE_DIVX_4;
        break;
      case 5:
        dec->decoder_format = VDEC_CODECTYPE_DIVX_5;
        break;
      case 6:
        dec->decoder_format = VDEC_CODECTYPE_DIVX_6;
        break;
      default:
        GST_ERROR ("OUCH");
    }
  } else if (gst_structure_has_name (structure, "video/x-h264")) {
    dec->decoder_format = VDEC_CODECTYPE_H264;
  } else {
    GST_ERROR ("OUCH");
  }

  GST_DEBUG ("set_format decoder_format is %d", dec->decoder_format);

  /*Initialize Decoder with codec type and resolution */
  ioctl_msg.in = &dec->decoder_format;
  ioctl_msg.out = NULL;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_SET_CODEC,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Set codec type failed");
    return FALSE;
  }

  /*Set the output format */
  ioctl_msg.in = &output_format;
  ioctl_msg.out = NULL;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_SET_OUTPUT_FORMAT,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Set output format failed");
    return FALSE;
  }

  /*To-Do */
  dec->video_resoultion.frame_width = Vinfo->width;
  dec->video_resoultion.frame_height = Vinfo->height;

  ioctl_msg.in = &dec->video_resoultion;
  ioctl_msg.out = NULL;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_SET_PICRES,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Set Resolution failed");
    return FALSE;
  }

  /*Get the Buffer requirements for input and output ports */
  dec->input_buffer.buffer_type = VDEC_BUFFER_TYPE_INPUT;
  ioctl_msg.in = NULL;
  ioctl_msg.out = &dec->input_buffer;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_GET_BUFFER_REQ,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Requesting for input buffer requirements failed");
    return FALSE;
  }

  GST_DEBUG
      ("input Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d ",
      dec->input_buffer.buffer_size, dec->input_buffer.mincount,
      dec->input_buffer.actualcount, dec->input_buffer.maxcount,
      dec->input_buffer.alignment, dec->input_buffer.buf_poolid,
      dec->input_buffer.meta_buffer_size);


  dec->input_buffer.buffer_type = VDEC_BUFFER_TYPE_INPUT;
  ioctl_msg.in = &dec->input_buffer;
  ioctl_msg.out = NULL;
  dec->input_buffer.actualcount = dec->input_buffer.mincount + 2;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_SET_BUFFER_REQ,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Set Buffer Requirements Failed");
    return FALSE;
  }


  dec->output_buffer.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
  ioctl_msg.in = NULL;
  ioctl_msg.out = &dec->output_buffer;

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_GET_BUFFER_REQ,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Requesting for output buffer requirements failed");
    return FALSE;
  }

  GST_DEBUG
      ("output_buffer Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d",
      dec->output_buffer.buffer_size, dec->output_buffer.mincount,
      dec->output_buffer.actualcount, dec->output_buffer.maxcount,
      dec->output_buffer.alignment, dec->output_buffer.buf_poolid,
      dec->output_buffer.meta_buffer_size);


  /*Allocate input and output buffers */
  if ((ion_allocate_buffer (VDEC_BUFFER_TYPE_INPUT, dec) == -1)) {
    GST_ERROR ("Error in input Buffer allocation");
    return FALSE;
  }
  if ((ion_allocate_buffer (VDEC_BUFFER_TYPE_OUTPUT, dec) == -1)) {
    GST_ERROR ("Error in output Buffer allocation");
    return FALSE;
  }
  if (dec->decoder_format == VDEC_CODECTYPE_H264)
    vdec_alloc_h264_mv (dec);
  /* Create the output state */
  dec->output_state =
      gst_video_decoder_set_output_state (decoder, fmt, Vinfo->width,
      Vinfo->height, dec->input_state);
  gst_video_decoder_negotiate (GST_VIDEO_DECODER (dec));
/* Create the pool start */
  if (dec->bufpool == NULL) {
    gst_qcvideodec_create_buf_pool (dec);
    gst_buffer_pool_set_active (dec->bufpool, TRUE);
  }                             //if(pool == NULL)
/* Create the pool End */
  gst_qcvideodec_start_decoder (dec);
/* send the header info */
  memcpy (dec->ptr_inputbuffer[0].bufferaddr, minfo.data, minfo.size);
  frameinfo.bufferaddr = dec->ptr_inputbuffer[0].bufferaddr;
  frameinfo.offset = 0;
  frameinfo.pmem_fd = dec->ptr_inputbuffer[0].pmem_fd;
  frameinfo.pmem_offset = dec->ptr_inputbuffer[0].offset;
  frameinfo.datalen = minfo.size;
  frameinfo.client_data =
      (struct vdec_bufferpayload *) &dec->ptr_inputbuffer[0];
  /*TODO: Time stamp needs to be updated */
  ioctl_msg.in = &frameinfo;
  ioctl_msg.out = NULL;
  //GST_ERROR ("input frame Client Data  address = %p and buff add : %p",frameinfo.client_data,frameinfo.bufferaddr);
  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_DECODE_FRAME, &ioctl_msg) < 0) {
    GST_ERROR ("Decoder frame failed");
    return FALSE;
  }
  sem_wait (&dec->sem_input_buf_done);
  return TRUE;
}

static gboolean
gst_qcvideodec_create_buf_pool (Gstqcvideodec * decoder)
{

  decoder->bufpool = gst_qc_video_buffer_pool_new ();
  if (decoder->bufpool == NULL) {
    GST_DEBUG ("pool is NULL, No memory ");
    return FALSE;
  }
  Gstqcvideobufpool *pool = GST_QCVIDEOBUFPOOL (decoder->bufpool);
  GstStructure *config = gst_buffer_pool_get_config (decoder->bufpool);

  gst_buffer_pool_config_set_params (config, NULL, 0,
      decoder->output_buffer.actualcount, decoder->output_buffer.maxcount);

  gst_video_info_init (&(pool->info));
  gst_video_info_set_format (&(pool->info), GST_VIDEO_FORMAT_NV12_64Z32,
      decoder->video_resoultion.frame_width,
      decoder->video_resoultion.frame_height);
  gst_buffer_pool_config_add_option (config, GST_BUFFER_POOL_OPTION_VIDEO_META);
  gst_buffer_pool_set_config (decoder->bufpool, config);

  return TRUE;
}

static gboolean
gst_qcvideodec_set_output_buffers (Gstqcvideodec * decoder)
{
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  struct vdec_fillbuffer_cmd fillbuffer;
  unsigned int index = 0;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);

  memset ((unsigned char *) &fillbuffer, 0,
      sizeof (struct vdec_fillbuffer_cmd));

  /*Push output Buffers */
  while (index < dec->output_buffer.actualcount) {
    fillbuffer.buffer.buffer_len = dec->ptr_outputbuffer[index].buffer_len;
    fillbuffer.buffer.bufferaddr = dec->ptr_outputbuffer[index].bufferaddr;
    fillbuffer.buffer.offset = dec->ptr_outputbuffer[index].offset;
    fillbuffer.buffer.pmem_fd = dec->ptr_outputbuffer[index].pmem_fd;
    fillbuffer.client_data = (void *) &dec->ptr_respbuffer[index];

    ioctl_msg.in = &fillbuffer;
    ioctl_msg.out = NULL;

    if (ioctl (dec->video_driver_fd,
            VDEC_IOCTL_FILL_OUTPUT_BUFFER, &ioctl_msg) < 0) {
      GST_ERROR ("Decoder frame failed");
      return FALSE;
    }
    index++;
  }
  GST_DEBUG ("output buffers set properly");
  return TRUE;
}

static gboolean
gst_qcvideodec_start_decoder (Gstqcvideodec * decoder)
{
  struct vdec_fillbuffer_cmd fillbuffer;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);

  memset ((unsigned char *) &fillbuffer, 0,
      sizeof (struct vdec_fillbuffer_cmd));

  if (dec == NULL) {
    return -1;
  }

  if (ioctl (dec->video_driver_fd, VDEC_IOCTL_CMD_START, NULL) < 0) {
    GST_ERROR ("Start failed");
    return FALSE;
  }

  GST_DEBUG ("Start Issued successfully waiting for Start Done");
  /*Wait for Start command response */
  sem_wait (&dec->sem_synchronize);
  gst_qcvideodec_set_output_buffers (dec);

  return TRUE;
}

static gboolean
gst_qcvideodec_close (GstVideoDecoder * decoder)
{
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder);
  if (dec == NULL) {
    return FALSE;
  }

  free_buffer (VDEC_BUFFER_TYPE_INPUT, dec);
  free_buffer (VDEC_BUFFER_TYPE_OUTPUT, dec);
  GST_DEBUG ("I/p and O/p ION freed successfully ");

  if (dec->ip_buf_ion_info) {
    free (dec->ip_buf_ion_info);
    dec->ip_buf_ion_info = NULL;
  }
  if (dec->op_buf_ion_info) {
    free (dec->op_buf_ion_info);
    dec->op_buf_ion_info = NULL;
  }
  if (dec->m_vdec_ion_devicefd > 0) {
    close (dec->m_vdec_ion_devicefd);
    dec->m_vdec_ion_devicefd = 0;
  }
  if (dec->video_driver_fd > 0) {
    close (dec->video_driver_fd);
    dec->video_driver_fd = 0;
  }
  sem_destroy (&dec->sem_input_buf_done);
  sem_destroy (&dec->sem_synchronize);

  GST_DEBUG ("************  qcvideodec is closed and return *********** ");
  return TRUE;
}


static GstFlowReturn
gst_qcvideodec_handle_frame (GstVideoDecoder * decoder_cxt,
    GstVideoCodecFrame * frame)
{
  GstFlowReturn res;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder_cxt);
  GST_VIDEO_DECODER_STREAM_UNLOCK (decoder_cxt);
  GST_DEBUG ("Enter with frame number : %d, TS: %"
      GST_TIME_FORMAT ", system_frame_number: %d, distance: %d, count: %d",
      frame->system_frame_number, GST_TIME_ARGS (frame->pts),
      frame->system_frame_number, frame->distance_from_sync, frame->ref_count);
  GST_VIDEO_DECODER_STREAM_LOCK (decoder_cxt);

  res = gst_qcvideodec_decode_buffer (dec, frame->input_buffer, frame);
  GST_VIDEO_DECODER_STREAM_UNLOCK (decoder_cxt);

  sem_wait (&dec->sem_input_buf_done);
  return res;
}

static GstFlowReturn
gst_qcvideodec_decode_buffer (Gstqcvideodec * dec, GstBuffer * buf,
    GstVideoCodecFrame * frame)
{
  Gstqcvideodec *decode_context;
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  struct vdec_input_frameinfo frameinfo = { NULL };
  GstMapInfo in_map_info = GST_MAP_INFO_INIT;

  decode_context = GST_QCVIDEODEC (dec);


  if (GST_VIDEO_CODEC_FRAME_IS_SYNC_POINT (frame)) {
    frameinfo.flags |= VDEC_BUFFERFLAG_SYNCFRAME;
  } else {
    frameinfo.flags |= VDEC_BUFFERFLAG_DECODEONLY;
  }

  gst_buffer_map (buf, &in_map_info, GST_MAP_READ);
  memcpy (temp_input_buffer->bufferaddr, in_map_info.data, in_map_info.size);
  frameinfo.bufferaddr = temp_input_buffer->bufferaddr;
  frameinfo.offset = 0;
  frameinfo.pmem_fd = temp_input_buffer->pmem_fd;
  frameinfo.pmem_offset = temp_input_buffer->offset;
  frameinfo.datalen = in_map_info.size;
  frameinfo.client_data = (struct vdec_bufferpayload *)
      temp_input_buffer;

  ioctl_msg.in = &frameinfo;
  ioctl_msg.out = NULL;
  if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_DECODE_FRAME,
          &ioctl_msg) < 0) {
    GST_ERROR ("Decoder frame failed");
    sem_post (&decode_context->sem_synchronize);
    return GST_FLOW_ERROR;
  }

  gst_buffer_unmap (buf, &in_map_info);
  gst_buffer_unref (buf);
  return GST_FLOW_OK;
}

int
free_buffer (enum vdec_buffer buffer_dir, Gstqcvideodec * decoder_context)
{
  unsigned int buffercount = 0, i = 0;
  struct vdec_bufferpayload *ptemp = NULL;
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  struct vdec_setbuffer_cmd setbuffers;
  Gstqcvideodec *dec = GST_QCVIDEODEC (decoder_context);

  if (dec == NULL) {
    return -1;
  }

  if (buffer_dir == VDEC_BUFFER_TYPE_INPUT && dec->ptr_inputbuffer) {
    buffercount = dec->input_buffer.actualcount;
    ptemp = dec->ptr_inputbuffer;

    for (i = 0; i < buffercount; i++) {
      if (ptemp[i].pmem_fd > 0 && ptemp[i].bufferaddr != NULL) {
        setbuffers.buffer_type = VDEC_BUFFER_TYPE_INPUT;
        memcpy (&setbuffers.buffer, &dec->ptr_inputbuffer[i],
            sizeof (struct vdec_bufferpayload));
        ioctl_msg.in = &setbuffers;
        ioctl_msg.out = NULL;

        if (ioctl (dec->video_driver_fd, VDEC_IOCTL_FREE_BUFFER,
                &ioctl_msg) < 0) {
          GST_ERROR ("Release output buffer failed in VCD");
        }

        munmap (ptemp[i].bufferaddr, ptemp[i].mmaped_size);
        ptemp[i].bufferaddr = NULL;
        close (ptemp[i].pmem_fd);
      }
    }
    free (dec->ptr_inputbuffer);
    dec->ptr_inputbuffer = NULL;
  }

  else if (buffer_dir == VDEC_BUFFER_TYPE_OUTPUT) {
    buffercount = dec->output_buffer.actualcount;
    ptemp = dec->ptr_outputbuffer;

    /* free mv buffers */
    if (h264_mv_buff.pmem_fd > 0) {
      if (ioctl (dec->video_driver_fd, VDEC_IOCTL_FREE_H264_MV_BUFFER,
              NULL) < 0)
        GST_ERROR ("VDEC_IOCTL_FREE_H264_MV_BUFFER failed");
      munmap (h264_mv_buff.buffer, h264_mv_buff.size);

      if (close (dec->h264_mv.fd_ion_data.fd)) {
        GST_ERROR ("ION: close(%d) failed", dec->h264_mv.fd_ion_data.fd);
      }
      /*   if(ioctl(dec->h264_mv.ion_device_fd,ION_IOC_FREE,
         &dec->h264_mv.ion_alloc_data.handle)) {
         GST_ERROR("ION: free failed, dev_fd = %d, handle = 0x%p",
         dec->h264_mv.ion_device_fd, dec->h264_mv.ion_alloc_data.handle);
         } */
      dec->h264_mv.ion_device_fd = -1;
      dec->h264_mv.ion_alloc_data.handle = NULL;
      dec->h264_mv.fd_ion_data.fd = -1;

      GST_DEBUG ("Cleaning H264_MV buffer of size %d", h264_mv_buff.size);
      h264_mv_buff.pmem_fd = -1;
      h264_mv_buff.offset = 0;
      h264_mv_buff.size = 0;
      h264_mv_buff.count = 0;
      h264_mv_buff.buffer = NULL;
    }
    /* free output buffers */
    ioctl_msg.in = &dec->output_buffer;
    ioctl_msg.out = NULL;

    for (i = 0; i < buffercount; i++) {
      if (ptemp[i].pmem_fd > 0 && ptemp[i].bufferaddr != NULL) {
        setbuffers.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
        memcpy (&setbuffers.buffer, &dec->ptr_outputbuffer[i],
            sizeof (struct vdec_bufferpayload));
        ioctl_msg.in = &setbuffers;
        ioctl_msg.out = NULL;

        if (ioctl (dec->video_driver_fd, VDEC_IOCTL_FREE_BUFFER,
                &ioctl_msg) < 0) {
          GST_ERROR ("Release output buffer failed in VCD");
        }
        munmap (ptemp[i].bufferaddr, ptemp[i].mmaped_size);
        ptemp[i].bufferaddr = NULL;
        close (ptemp[i].pmem_fd);
      }
    }
    if (dec->ptr_respbuffer) {
      free (dec->ptr_respbuffer);
      dec->ptr_respbuffer = NULL;
    }
    if (dec->ptr_outputbuffer) {
      free (dec->ptr_outputbuffer);
      dec->ptr_outputbuffer = NULL;
    }
  }
  return 1;
}

static int
ion_allocate_buffer (enum vdec_buffer buffer_dir, Gstqcvideodec * decoder_cxt)
{
  struct vdec_setbuffer_cmd setbuffers;
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  unsigned int i = 0;
  int rc = -1;
  int fd = -1;
  int pmem_fd = -1;
  unsigned char *buf_addr = NULL;

  Gstqcvideodec *decode_context = GST_QCVIDEODEC (decoder_cxt);

  if (decode_context == NULL) {
    GST_ERROR ("allocate_buffer: context is NULL");
    return -1;
  }

  if (buffer_dir == VDEC_BUFFER_TYPE_INPUT) {
    /*Check if buffers are allocated */
    if (decode_context->ptr_inputbuffer != NULL) {
      GST_ERROR ("allocate_buffer: decode_context->ptr_inputbuffer is set");
      return -1;
    }
    GST_DEBUG ("Allocate i/p buffer Header: Cnt(%d) Sz(%d) ",
        decode_context->input_buffer.actualcount,
        decode_context->input_buffer.buffer_size);

    decode_context->ptr_inputbuffer = (struct vdec_bufferpayload *)
        calloc ((sizeof (struct vdec_bufferpayload)),
        decode_context->input_buffer.actualcount);
    if (decode_context->ptr_inputbuffer == NULL) {
      GST_ERROR ("ptr_inputbuffer allocated FAIL");
      return -1;
    }
    decode_context->ip_buf_ion_info = (struct vdec_ion *)
        calloc ((sizeof (struct vdec_ion)),
        decode_context->input_buffer.actualcount);

    if (decode_context->ip_buf_ion_info == NULL) {
      GST_ERROR ("ip_buf_ion_info allocated FAIL");
      return -1;
    }
    for (i = 0; i < decode_context->input_buffer.actualcount; i++) {
      decode_context->ptr_inputbuffer[i].pmem_fd = -1;
      decode_context->ip_buf_ion_info[i].ion_device_fd = -1;
    }

    for (i = 0; i < decode_context->input_buffer.actualcount; i++) {
      struct ion_allocation_data *alloc_data =
          &decode_context->ip_buf_ion_info[i].ion_alloc_data;
      struct ion_fd_data *fd_data =
          &decode_context->ip_buf_ion_info[i].fd_ion_data;
      fd = decode_context->m_vdec_ion_devicefd;
      alloc_data->len = decode_context->input_buffer.buffer_size;
      alloc_data->flags = 0x1;
      alloc_data->align = 2048;
      alloc_data->heap_mask = 0x2000000;

      rc = ioctl (fd, ION_IOC_ALLOC, alloc_data);
      if (rc || !alloc_data->handle) {
        GST_ERROR ("ION ALLOC failed, fd = %d, rc = %d, handle = 0x%p, "
            "errno ", fd, rc, alloc_data->handle);
        alloc_data->handle = NULL;
        return -1;
      }
      GST_LOG ("ION memory allocated successfully ");
      fd_data->handle = alloc_data->handle;
      rc = ioctl (fd, ION_IOC_MAP, fd_data);
      if (rc) {
        GST_ERROR ("ION MAP failed, fd = %d, handle = 0x%p, errno =",
            fd, fd_data->handle);
        fd_data->fd = -1;
        return -1;
      }

      decode_context->ip_buf_ion_info[i].ion_device_fd = fd;
      pmem_fd = decode_context->ip_buf_ion_info[i].fd_ion_data.fd;
      buf_addr = (unsigned char *) mmap (NULL,
          decode_context->input_buffer.buffer_size,
          PROT_READ | PROT_WRITE, MAP_SHARED, pmem_fd, 0);
      if (buf_addr == MAP_FAILED) {
        pmem_fd = -1;
        GST_ERROR ("Map Failed to allocate input buffer");
        return -1;
      }
      decode_context->ptr_inputbuffer[i].bufferaddr = buf_addr;
      decode_context->ptr_inputbuffer[i].pmem_fd = pmem_fd;
      decode_context->ptr_inputbuffer[i].buffer_len =
          decode_context->input_buffer.buffer_size;
      decode_context->ptr_inputbuffer[i].mmaped_size =
          decode_context->input_buffer.buffer_size;
      decode_context->ptr_inputbuffer[i].offset = 0;

      setbuffers.buffer_type = VDEC_BUFFER_TYPE_INPUT;
      memcpy (&setbuffers.buffer, &decode_context->ptr_inputbuffer[i],
          sizeof (struct vdec_bufferpayload));
      ioctl_msg.in = &setbuffers;
      ioctl_msg.out = NULL;

      if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_SET_BUFFER,
              &ioctl_msg) < 0) {
        GST_ERROR ("Set Buffers Failed");
        return -1;
      }
      GST_DEBUG ("input  ion_alloc: buf_addr = %p, len = %d, size = %d, ",
          buf_addr, decode_context->input_buffer.buffer_size,
          decode_context->input_buffer.buffer_size);
    }                           /* for loop */
    GST_DEBUG ("Input buffers ion_allocate_buffer: Success");
  }                             /* VDEC_BUFFER_TYPE_INPUT  */
  if (buffer_dir == VDEC_BUFFER_TYPE_OUTPUT) {
    GST_DEBUG ("Allocate o/p buffer Header: Cnt(%d) Sz(%d)",
        decode_context->output_buffer.actualcount,
        decode_context->output_buffer.buffer_size);

    decode_context->ptr_outputbuffer = (struct vdec_bufferpayload *)
        calloc (sizeof (struct vdec_bufferpayload),
        decode_context->output_buffer.actualcount);

    decode_context->ptr_respbuffer = (struct vdec_output_frameinfo *)
        calloc (sizeof (struct vdec_output_frameinfo),
        decode_context->output_buffer.actualcount);

    decode_context->op_buf_ion_info = (struct vdec_ion *)
        calloc (sizeof (struct vdec_ion),
        decode_context->output_buffer.actualcount);

    for (i = 0; i < decode_context->output_buffer.actualcount; i++) {
      decode_context->ptr_outputbuffer[i].pmem_fd = -1;
      decode_context->op_buf_ion_info[i].ion_device_fd = -1;
      /*Create a mapping between buffers */
      decode_context->ptr_respbuffer[i].client_data = (void *)
          &decode_context->ptr_outputbuffer[i];
    }
    for (i = 0; i < decode_context->output_buffer.actualcount; i++) {
      struct ion_allocation_data *alloc_data =
          &decode_context->op_buf_ion_info[i].ion_alloc_data;
      struct ion_fd_data *fd_data =
          &decode_context->op_buf_ion_info[i].fd_ion_data;
      fd = decode_context->m_vdec_ion_devicefd;
      alloc_data->len = decode_context->output_buffer.buffer_size;
      alloc_data->flags = 0x1;
      alloc_data->align = 8192;
      alloc_data->heap_mask = 0x2000000;
      rc = ioctl (fd, ION_IOC_ALLOC, alloc_data);

      if (rc || !alloc_data->handle) {
        GST_ERROR ("ION ALLOC failed, fd = %d, rc = %d, handle = 0x%p, "
            "errno ", fd, rc, alloc_data->handle);
        alloc_data->handle = NULL;
        return -1;
      }
      fd_data->handle = alloc_data->handle;
      rc = ioctl (fd, ION_IOC_MAP, fd_data);
      if (rc) {
        GST_ERROR ("ION MAP failed, fd = %d, handle = 0x%p, errno",
            fd, fd_data->handle);
        fd_data->fd = -1;
        return -1;
      }

      decode_context->op_buf_ion_info[i].ion_device_fd = fd;
      pmem_fd = decode_context->op_buf_ion_info[i].fd_ion_data.fd;
      buf_addr = (unsigned char *) mmap (NULL,
          decode_context->output_buffer.buffer_size,
          PROT_READ | PROT_WRITE, MAP_SHARED, pmem_fd, 0);
      if (buf_addr == MAP_FAILED) {
        pmem_fd = -1;
        GST_ERROR ("Map Failed to allocate input buffer");
        return -1;
      }
      decode_context->ptr_outputbuffer[i].bufferaddr = buf_addr;
      decode_context->ptr_outputbuffer[i].pmem_fd = pmem_fd;
      decode_context->ptr_outputbuffer[i].buffer_len =
          decode_context->output_buffer.buffer_size;
      decode_context->ptr_outputbuffer[i].mmaped_size =
          decode_context->output_buffer.buffer_size;
      decode_context->ptr_outputbuffer[i].offset = 0;

      setbuffers.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
      memcpy (&setbuffers.buffer, &decode_context->ptr_outputbuffer[i],
          sizeof (struct vdec_bufferpayload));
      ioctl_msg.in = &setbuffers;
      ioctl_msg.out = NULL;

      if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_SET_BUFFER,
              &ioctl_msg) < 0) {
        GST_ERROR ("Set Buffers Failed");
        return -1;
      }
      GST_INFO ("output ion_alloc: buf_addr = %p, len = %d, size = %d, ",
          buf_addr, decode_context->output_buffer.buffer_size,
          decode_context->output_buffer.buffer_size);

    }                           /* for loop */
    GST_DEBUG ("output ion_allocate_buffer: Success");
  }                             /* VDEC_BUFFER_TYPE_OUTPUT */
  return 1;
}

int
vdec_alloc_h264_mv (Gstqcvideodec * decoder_cxt)
{
  int pmem_fd = -1, fd = -1, rc = -1;
  int size;
  void *buf_addr = NULL;
  struct vdec_ioctl_msg ioctl_msg;
  // struct pmem_allocation allocation;
  struct vdec_h264_mv h264_mv;
  struct vdec_mv_buff_size mv_buff_size;
//  struct video_decoder_context *decode_context = NULL;
  Gstqcvideodec *decode_context = GST_QCVIDEODEC (decoder_cxt);
  // decode_context = (struct video_decoder_context *) context;

  decode_context->video_resoultion.stride =
      decode_context->video_resoultion.frame_width;
  decode_context->video_resoultion.scan_lines =
      decode_context->video_resoultion.frame_height;

  mv_buff_size.width = decode_context->video_resoultion.stride;
  mv_buff_size.height = decode_context->video_resoultion.scan_lines >> 2;

  ioctl_msg.in = NULL;
  ioctl_msg.out = (void *) &mv_buff_size;

  if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_GET_MV_BUFFER_SIZE,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("GET_MV_BUFFER_SIZE Failed for width: %d, Height %d",
        mv_buff_size.width, mv_buff_size.height);
    return -1;
  }

  GST_INFO ("GET_MV_BUFFER_SIZE returned: Size: %d and alignment: %d",
      mv_buff_size.size, mv_buff_size.alignment);

  size = mv_buff_size.size * decode_context->output_buffer.actualcount;
  // alignment = mv_buff_size.alignment;

  GST_INFO
      ("Entered vdec_alloc_h264_mv act_width: %d, act_height: %d, size: %d, alignment %d",
      decode_context->video_resoultion.frame_width,
      decode_context->video_resoultion.frame_height, size,
      mv_buff_size.alignment);

  struct ion_allocation_data *alloc_data =
      &decode_context->h264_mv.ion_alloc_data;
  struct ion_fd_data *fd_data = &decode_context->h264_mv.fd_ion_data;
  fd = decode_context->m_vdec_ion_devicefd;
  alloc_data->len = size;
  alloc_data->flags = 0x1;
  alloc_data->align = 8196;
  alloc_data->heap_mask = 0x2000000;
  rc = ioctl (fd, ION_IOC_ALLOC, alloc_data);
  if (rc || !alloc_data->handle) {
    GST_ERROR ("ION ALLOC failed, fd = %d, rc = %d, handle = 0x%p, "
        "errno ", fd, rc, alloc_data->handle);
    alloc_data->handle = NULL;
    return -1;
  }
  fd_data->handle = alloc_data->handle;
  rc = ioctl (fd, ION_IOC_MAP, fd_data);
  if (rc) {
    GST_ERROR ("ION MAP failed, fd = %d, handle = 0x%p, errno =",
        fd, fd_data->handle);
    fd_data->fd = -1;
    return -1;
  }
  decode_context->h264_mv.ion_device_fd = fd;
  pmem_fd = decode_context->h264_mv.fd_ion_data.fd;

  buf_addr = mmap (NULL, size, PROT_READ | PROT_WRITE, MAP_SHARED, pmem_fd, 0);

  if (buf_addr == (void *) MAP_FAILED) {
    pmem_fd = -1;
    GST_ERROR ("Error returned in allocating recon buffers buf_addr: %p",
        buf_addr);
    return -1;
  }
  GST_INFO ("Allocated virt:%p, FD: %d of size %d count: %d", buf_addr,
      pmem_fd, size, decode_context->output_buffer.actualcount);

  h264_mv.size = size;
  h264_mv.count = decode_context->output_buffer.actualcount;
  h264_mv.pmem_fd = pmem_fd;
  h264_mv.offset = 0;

  ioctl_msg.in = (void *) &h264_mv;
  ioctl_msg.out = NULL;

  if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_SET_H264_MV_BUFFER,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("Failed to set the H264_mv_buffers");
    return -1;
  }

  h264_mv_buff.buffer = (unsigned char *) buf_addr;
  h264_mv_buff.size = size;
  h264_mv_buff.count = decode_context->output_buffer.actualcount;
  h264_mv_buff.offset = 0;
  h264_mv_buff.pmem_fd = pmem_fd;
  GST_INFO ("Saving virt:%p, FD: %d of size %d count: %d", h264_mv_buff.buffer,
      h264_mv_buff.pmem_fd, h264_mv_buff.size,
      decode_context->output_buffer.actualcount);
  return 1;
}

int
allocate_reconfig_buff (Gstqcvideodec * dec)
{
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
//  struct video_decoder_context *decode_context = NULL;
  struct vdec_input_frameinfo frameinfo;
  struct vdec_fillbuffer_cmd fillbuffer;
  Gstqcvideodec *decode_context = GST_QCVIDEODEC (dec);

  memset ((unsigned char *) &frameinfo, 0,
      sizeof (struct vdec_input_frameinfo));
  memset ((unsigned char *) &fillbuffer, 0,
      sizeof (struct vdec_fillbuffer_cmd));

//  decode_context = (struct video_decoder_context *) context;
  decode_context->output_buffer.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
  ioctl_msg.in = NULL;
  ioctl_msg.out = &decode_context->output_buffer;

  if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_GET_BUFFER_REQ,
          (void *) &ioctl_msg) < 0) {
    GST_ERROR ("2nd time Requesting for output buffer requirements failed");
    return -1;
  }

  GST_DEBUG
      ("2nd time output_buffer Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d",
      decode_context->output_buffer.buffer_size,
      decode_context->output_buffer.mincount,
      decode_context->output_buffer.actualcount,
      decode_context->output_buffer.maxcount,
      decode_context->output_buffer.alignment,
      decode_context->output_buffer.buf_poolid,
      decode_context->output_buffer.meta_buffer_size);
  if (decode_context->output_buffer.actualcount <
      decode_context->output_buffer.maxcount / 2) {

    decode_context->output_buffer.actualcount = (decode_context->output_buffer.maxcount / 2);   //decode_context->output_buffer.actualcount + decode_context->output_buffer.mincount;
    decode_context->output_buffer.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
    ioctl_msg.in = &decode_context->output_buffer;
    ioctl_msg.out = NULL;

    if (ioctl (dec->video_driver_fd, VDEC_IOCTL_SET_BUFFER_REQ,
            (void *) &ioctl_msg) < 0) {
      GST_ERROR ("Set Buffer Requirements Failed");
      return FALSE;
    }
    decode_context->output_buffer.buffer_type = VDEC_BUFFER_TYPE_OUTPUT;
    ioctl_msg.in = NULL;
    ioctl_msg.out = &decode_context->output_buffer;

    if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_GET_BUFFER_REQ,
            (void *) &ioctl_msg) < 0) {
      GST_ERROR ("3rd time Requesting for output buffer requirements failed");
      return -1;
    }
    GST_DEBUG
        ("3rd time output_buffer Size=%d min count =%d actual count = %d, maxcount = %d,alignment = %d,buf_poolid= %d,meta_buffer_size=%d",
        decode_context->output_buffer.buffer_size,
        decode_context->output_buffer.mincount,
        decode_context->output_buffer.actualcount,
        decode_context->output_buffer.maxcount,
        decode_context->output_buffer.alignment,
        decode_context->output_buffer.buf_poolid,
        decode_context->output_buffer.meta_buffer_size);

  }
  if (decode_context->decoder_format == VDEC_CODECTYPE_H264)
    vdec_alloc_h264_mv (decode_context);

  if ((ion_allocate_buffer (VDEC_BUFFER_TYPE_OUTPUT, decode_context) == -1)) {
    GST_ERROR ("Error in output Buffer allocation");
    return -1;
  }
  gst_qcvideodec_set_output_buffers (decode_context);
  gst_buffer_pool_set_active (decode_context->bufpool, FALSE);
  gst_object_unref (decode_context->bufpool);

  gst_qcvideodec_create_buf_pool (decode_context);
  gst_buffer_pool_set_active (decode_context->bufpool, TRUE);

  return 1;
}

//Reference from gstreamer/gst-plugins-good/tree/sys/v4l2/gstv4l2videodec.c
static GstVideoCodecFrame *
gst_qc_video_dec_get_oldest_frame (GstVideoDecoder * decoder)
{
  GstVideoCodecFrame *frame = NULL;
  GList *frames, *l;
  gint count = 0;

  frames = gst_video_decoder_get_frames (decoder);

  for (l = frames; l != NULL; l = l->next) {
    GstVideoCodecFrame *f = l->data;

    if (!frame || frame->pts > f->pts)
      frame = f;

    count++;
  }

  if (frame) {
    GST_LOG ("Oldest frame is %d %" GST_TIME_FORMAT " and %d frames left",
        frame->system_frame_number, GST_TIME_ARGS (frame->pts), count - 1);
    gst_video_codec_frame_ref (frame);
  }

  g_list_free_full (frames, (GDestroyNotify) gst_video_codec_frame_unref);

  return frame;
}

static void *
video_thread (void *context)
{
//   struct Gstqcvideodec *decode_context = NULL;
  struct vdec_ioctl_msg ioctl_msg = { NULL, NULL };
  struct vdec_msginfo vdec_msg;
  struct vdec_fillbuffer_cmd fillbuffer;
  struct vdec_output_frameinfo *outputbuffer = NULL;
  struct vdec_bufferpayload *dec_output_buf = NULL;
  GstVideoDecoder *decoder_cxt = (GstVideoDecoder *) context;
  Gstqcvideodec *decode_context = GST_QCVIDEODEC (decoder_cxt);
  GstVideoCodecFrame *g_frame = NULL;
  int frm_count = 0;
  GstBuffer *gst_local_buf = NULL;
  Gstqcvideobufpool *derivedpool = NULL;
  GstFlowReturn flow;

  if (decode_context == NULL) {
    GST_ERROR ("video thread recieved NULL context");
    pthread_exit(NULL);
  }

  while (1) {

    ioctl_msg.in = NULL;
    ioctl_msg.out = (void *) &vdec_msg;
    if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_GET_NEXT_MSG,
            (void *) &ioctl_msg) < 0) {
      GST_ERROR (" Error in ioctl read next msg");
    } else {
      GST_LOG ("async_thread ioctl received command : %d", vdec_msg.msgcode);

      switch (vdec_msg.msgcode) {
        case VDEC_MSG_EVT_HW_ERROR:
          GST_ERROR ("FATAL ERROR ");
          break;
        case VDEC_MSG_RESP_INPUT_FLUSHED:
          GST_LOG ("Received command: VDEC_MSG_RESP_INPUT_FLUSHED");
          break;
        case VDEC_MSG_RESP_OUTPUT_FLUSHED:
          GST_LOG ("Received command: VDEC_MSG_RESP_OUTPUT_FLUSHED");
          break;
        case VDEC_MSG_RESP_START_DONE:
          GST_DEBUG ("Received command: VDEC_MSG_RESP_START_DONE");
          sem_post (&decode_context->sem_synchronize);
          break;

        case VDEC_MSG_RESP_STOP_DONE:
          GST_DEBUG ("Received command: VDEC_MSG_RESP_STOP_DONE");
          sem_post (&decode_context->sem_synchronize);
          break;

        case VDEC_MSG_RESP_INPUT_BUFFER_DONE:
          GST_LOG ("Received command: VDEC_MSG_RESP_INPUT_BUFFER_DONE");
          // GST_VIDEO_DECODER_STREAM_LOCK (decoder_cxt);
          temp_input_buffer =
              (struct vdec_bufferpayload *) vdec_msg.
              msgdata.input_frame_clientdata;
          if (temp_input_buffer == NULL) {
            GST_ERROR ("Input buffer address is NULL");
            sem_post (&decode_context->sem_synchronize);
            //  break;
          }
          /*To-do read the next frame */
          GST_LOG
              ("VDEC_MSG_RESP_INPUT_BUFFER_DONE Input buffer done for index ");

          sem_post (&decode_context->sem_input_buf_done);
          // GST_VIDEO_DECODER_STREAM_LOCK (decoder_cxt);
          break;

        case VDEC_MSG_EVT_CONFIG_CHANGED:
          GST_DEBUG ("Received command: VDEC_MSG_EVT_CONFIG_CHANGED");
          reconfig = 1;
          ioctl_msg.in = &decode_context->output_buffer;
          ioctl_msg.out = NULL;
          if (ioctl (decode_context->video_driver_fd, VDEC_IOCTL_CMD_FLUSH,
                  &ioctl_msg) < 0) {
            GST_ERROR ("Flush Port  Failed ");
          }
          (void) free_buffer (VDEC_BUFFER_TYPE_OUTPUT, decode_context);
          break;

        case VDEC_MSG_RESP_OUTPUT_BUFFER_DONE:
          GST_LOG ("Received command: VDEC_MSG_RESP_OUTPUT_BUFFER_DONE");

          GST_VIDEO_DECODER_STREAM_LOCK (decoder_cxt);
          outputbuffer =
              (struct vdec_output_frameinfo *) vdec_msg.msgdata.
              output_frame.client_data;

          // TODO: ndec: outputbuffer cannot be null, assert instead ?
          outputbuffer->bufferaddr = vdec_msg.msgdata.output_frame.bufferaddr;
          outputbuffer->framesize.bottom =
              vdec_msg.msgdata.output_frame.framesize.bottom;
          outputbuffer->framesize.left =
              vdec_msg.msgdata.output_frame.framesize.left;
          outputbuffer->framesize.right =
              vdec_msg.msgdata.output_frame.framesize.right;
          outputbuffer->framesize.top =
              vdec_msg.msgdata.output_frame.framesize.top;
          outputbuffer->framesize = vdec_msg.msgdata.output_frame.framesize;
          outputbuffer->len = vdec_msg.msgdata.output_frame.len;
          outputbuffer->pic_type = vdec_msg.msgdata.output_frame.pic_type;
          outputbuffer->time_stamp = vdec_msg.msgdata.output_frame.time_stamp;

          if (outputbuffer == NULL || outputbuffer->bufferaddr == NULL ||
              outputbuffer->client_data == NULL) {
            GST_ERROR ("Output buffer is bad");
            GST_ERROR ("Values outputbuffer = %p", outputbuffer);
            if (outputbuffer != NULL) {
              GST_ERROR ("Values outputbuffer->bufferaddr = %p",
                  outputbuffer->bufferaddr);
              GST_ERROR ("Values outputbuffer->client_data = %p",
                  outputbuffer->client_data);
            }
            sem_post (&decode_context->sem_synchronize);
            break;
          }

          dec_output_buf = (struct vdec_bufferpayload *)
              outputbuffer->client_data;
          if (outputbuffer->len == 0) {
            Completed_frm_decode = TRUE;
            GST_INFO ("Total decoded buffers are : %d", frm_count);
            sem_post (&decode_context->sem_synchronize);
            //     break;
          }
          g_frame =
              gst_qc_video_dec_get_oldest_frame (GST_VIDEO_DECODER
              (decode_context));
          // g_frame = gst_video_decoder_get_frame(GST_VIDEO_DECODER(decode_context),frm_count);
          GST_DEBUG
              ("VDEC_MSG_RESP_OUTPUT_BUFFER_DONE  for the buffer index : %d and len : %d",
              frm_count, outputbuffer->len);
          GST_DEBUG ("Frame number : %d, TS: %"
              GST_TIME_FORMAT ", system_frame_number: %d",
              g_frame->system_frame_number, GST_TIME_ARGS (g_frame->pts),
              g_frame->system_frame_number);

          if (g_frame != NULL) {
/*            ret = gst_video_decoder_allocate_output_frame(GST_VIDEO_DECODER(decode_context),g_frame);
            if (ret == GST_FLOW_OK && gst_buffer_map (g_frame->output_buffer, &map, GST_MAP_WRITE))
            {
	      // copy the luma plane
	      int y_size = GST_ROUND_UP_128(decode_context->video_resoultion.frame_width) *
		      GST_ROUND_UP_32(decode_context->video_resoultion.frame_height);

	      int c_size = GST_ROUND_UP_128(decode_context->video_resoultion.frame_width) *
		      GST_ROUND_UP_32(decode_context->video_resoultion.frame_height/2);

	      memcpy (map.data, outputbuffer->bufferaddr, GST_ROUND_UP_N(y_size, 4096));

	      // copy the chroma plane
	      memcpy (map.data + GST_ROUND_UP_N(y_size,4096),
		      outputbuffer->bufferaddr + GST_ROUND_UP_N(y_size, 8192),
		      GST_ROUND_UP_N(c_size, 4096));

              ret = gst_video_decoder_finish_frame(GST_VIDEO_DECODER(decode_context),g_frame);
              gst_buffer_unmap(g_frame->output_buffer, &map);
              gst_buffer_unref(g_frame->output_buffer);
            }
*/

            derivedpool = GST_QCVIDEOBUFPOOL (decode_context->bufpool);
            flow =
                gst_buffer_pool_acquire_buffer (decode_context->bufpool,
                &gst_local_buf, NULL);
            if (flow != GST_FLOW_OK) {
              GST_ERROR
                  ("gst_buffer_pool_acquire_buffer failed and GST_FLOW_Error ");
              pthread_exit(NULL);
            }
            gst_buffer_insert_memory (gst_local_buf, -1,
                gst_dmabuf_allocator_alloc (derivedpool->allocator,
                    (uint32_t) dec_output_buf->pmem_fd, outputbuffer->len));
            g_frame->output_buffer = gst_local_buf;
            flow =
                gst_video_decoder_finish_frame (GST_VIDEO_DECODER
                (decode_context), g_frame);
            if (flow != GST_FLOW_OK) {
              GST_ERROR ("finish_frame FAILED in video decoder: %d", flow);
            }
            gst_buffer_unref (gst_local_buf);
          }
          frm_count++;
          fillbuffer.buffer.buffer_len = dec_output_buf->buffer_len;
          fillbuffer.buffer.bufferaddr = dec_output_buf->bufferaddr;
          fillbuffer.buffer.offset = dec_output_buf->offset;
          fillbuffer.buffer.pmem_fd = dec_output_buf->pmem_fd;
          fillbuffer.client_data = (void *) outputbuffer;

          ioctl_msg.in = &fillbuffer;
          ioctl_msg.out = NULL;

          if (ioctl (decode_context->video_driver_fd,
                  VDEC_IOCTL_FILL_OUTPUT_BUFFER, &ioctl_msg) < 0) {
            GST_ERROR ("Decoder frame failed");
            pthread_exit(NULL);
          }
          GST_VIDEO_DECODER_STREAM_UNLOCK (decoder_cxt);
          break;

        case VDEC_MSG_RESP_FLUSH_INPUT_DONE:
          GST_LOG ("Received command: VDEC_MSG_RESP_FLUSH_INPUT_DONE");
          sem_post (&decode_context->sem_synchronize);
          break;

        case VDEC_MSG_RESP_FLUSH_OUTPUT_DONE:
          GST_LOG ("Received command: VDEC_MSG_RESP_FLUSH_OUTPUT_DONE");
          if (reconfig == 1) {
            allocate_reconfig_buff (decode_context);
            GST_LOG ("reconfig set to 0");
            reconfig = 0;
          } else
            sem_post (&decode_context->sem_synchronize);
          break;
        default:
          GST_ERROR ("Video thread default case, received: %d",
              vdec_msg.msgcode);
      }

      if (vdec_msg.msgcode == VDEC_MSG_RESP_STOP_DONE) {
        GST_INFO ("Video_thread is going to close");
        pthread_exit(NULL);
      }
    }

  }
}

/* initialize the qcvideodec's class */
static void
gst_qcvideodec_class_init (GstqcvideodecClass * klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GstElementClass *gstelement_class = (GstElementClass *) klass;
  GstVideoDecoderClass *video_decoder_class = GST_VIDEO_DECODER_CLASS (klass);

  GST_DEBUG ("class_init Enter");
  gobject_class->set_property = gst_qcvideodec_set_property;
  gobject_class->get_property = gst_qcvideodec_get_property;

  g_object_class_install_property (gobject_class, PROP_SILENT,
      g_param_spec_boolean ("silent", "Silent", "Produce verbose output ?",
          FALSE, G_PARAM_READWRITE));

  gst_element_class_set_details_simple (gstelement_class,
      "qcvideodec",
      "Codec/Decoder/Video",
      "QCOM mpeg4 decodeR", "c_kamuju <<user@hostname.org>>");

  video_decoder_class->start = GST_DEBUG_FUNCPTR (gst_qcvideodec_start);
  video_decoder_class->stop = GST_DEBUG_FUNCPTR (gst_qcvideodec_stop);
  video_decoder_class->set_format =
      GST_DEBUG_FUNCPTR (gst_qcvideodec_set_format);
  video_decoder_class->open = GST_DEBUG_FUNCPTR (gst_qcvideodec_open);
  video_decoder_class->close = GST_DEBUG_FUNCPTR (gst_qcvideodec_close);
  video_decoder_class->handle_frame =
      GST_DEBUG_FUNCPTR (gst_qcvideodec_handle_frame);
  video_decoder_class->finish = GST_DEBUG_FUNCPTR (gst_qcvideodec_finish);
  //video_decoder_class->parse = GST_DEBUG_FUNCPTR (gst_qcvideodec_parse);


  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&src_factory));
  gst_element_class_add_pad_template (gstelement_class,
      gst_static_pad_template_get (&sink_factory));

  GST_DEBUG_CATEGORY_INIT (gst_qcvideodec_debug, "mpeg4dec", 0,
      "QC MPEG-4 Video Decoder");
  GST_DEBUG ("class_init Exit");
}

/* initialize the new element
 * instantiate pads and add them to element
 * set pad calback functions
 * initialize instance structure
 */
static void
gst_qcvideodec_init (Gstqcvideodec * filter)
{
  GST_DEBUG ("qcvideodec_init Enter");
  filter->sinkpad = gst_pad_new_from_static_template (&sink_factory, "qcsink");

  GST_PAD_SET_PROXY_CAPS (filter->sinkpad);
  gst_element_add_pad (GST_ELEMENT (filter), filter->sinkpad);

  filter->srcpad = gst_pad_new_from_static_template (&src_factory, "qcsrc");

  gst_pad_set_active (filter->srcpad, TRUE);
  GST_PAD_SET_PROXY_CAPS (filter->srcpad);
  gst_element_add_pad (GST_ELEMENT (filter), filter->srcpad);
  gst_pad_set_active (filter->srcpad, TRUE);

  filter->silent = FALSE;
}

static void
gst_qcvideodec_set_property (GObject * object, guint prop_id,
    const GValue * value, GParamSpec * pspec)
{
  Gstqcvideodec *filter = GST_QCVIDEODEC (object);
  switch (prop_id) {
    case PROP_SILENT:
      filter->silent = g_value_get_boolean (value);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static void
gst_qcvideodec_get_property (GObject * object, guint prop_id,
    GValue * value, GParamSpec * pspec)
{
  Gstqcvideodec *filter = GST_QCVIDEODEC (object);
  switch (prop_id) {
    case PROP_SILENT:
      g_value_set_boolean (value, filter->silent);
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}


/* entry point to initialize the plug-in
 * initialize the plug-in itself
 * register the element factories and other features
 */
static gboolean
qcvideodec_init (GstPlugin * qcvideodec)
{
  /* debug category for fltering log messages
   *
   * exchange the string 'Template qcvideodec' with your description
   */
  GST_DEBUG_CATEGORY_INIT (gst_qcvideodec_debug, "qcvideodec",
      0, "Template qcvideodec");

  // gst_video_decoder_set_packetized (GST_VIDEO_DECODER (qcvideodec), FALSE);
  // gst_video_decoder_set_needs_format (GST_VIDEO_DECODER (qcvideodec), TRUE);

  return gst_element_register (qcvideodec, "qcvideodec", GST_RANK_PRIMARY + 1,
      GST_TYPE_QCVIDEODEC);
}

/* PACKAGE: this is usually set by autotools depending on some _INIT macro
 * in configure.ac and then written into and defined in config.h, but we can
 * just set it ourselves here in case someone doesn't use autotools to
 * compile this code. GST_PLUGIN_DEFINE needs PACKAGE to be defined.
 */
#ifndef PACKAGE
#define PACKAGE "myfirstqcvideodec"
#endif

/* gstreamer looks for this structure to register qcvideodecs
 *
 * exchange the string 'Template qcvideodec' with your qcvideodec description
 */
GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    qcvideodec,
    "Template qcvideodec",
    qcvideodec_init, VERSION, "LGPL", "GStreamer", "http://gstreamer.net/")
